'use strict';

module.exports = {
    // default
    en: {
        '\\Sent': 'Sent Mail',
        '\\Trash': 'Trash',
        '\\Junk': 'Junk',
        '\\Drafts': 'Drafts',
        '\\Archive': 'Archive'
    },
    // estonian
    et: {
        '\\Sent': 'Saadetud kirjad',
        '\\Trash': 'Prügikast',
        '\\Junk': 'Rämpspost',
        '\\Drafts': 'Mustandid',
        '\\Archive': 'Arhiiv'
    },
    es: {
        '\\Sent': 'Enviado',
        '\\Trash': 'Basura',
        '\\Junk': 'Spam',
        '\\Drafts': 'Borradores',
        '\\Archive': 'Archivado'
    }
};
